import {type_check} from './Functions/type_checker.js';
import {InvalidProps} from './Exceptions/InvalidProps.js';
import interpolate from './Functions/interpolate.js';



export const React = {
    createElement:(tageOrElement, props, children) => {
        if (typeof tageOrElement === 'function'){
            if(!type_check(props,tageOrElement.propsType)) throw new InvalidProps();
            return new tageOrElement(props).render();
        }else{
            let element = document.createElement(tageOrElement);
            for (const attribute in props){
                if (attribute === 'click' || attribute === 'change')
                    element.addEventListener(attribute,props[attribute]);
                else
                    element.setAttribute(attribute, props[attribute]);
            }
            for (const child in children) {
                if (typeof children[child] === 'string') {
                        children[child] = document.createTextNode(children[child].interpolate(props));

                }
                element.appendChild(children[child]);

            }
            return element;
        }
    },
    Component: class {
        constructor(props){
            this.props = props;
            this.state = {};
        }
            
        receiveData = (props) => {
          this.props = props;
        };
      
        setState = (setState) => this.state = Object.assign({}, setState);
      
        needUpdate = (newProps,newState) => JSON.stringify(newProps) !== JSON.stringify(this.props) 
        || JSON.stringify(newState) !== JSON.stringify(this.state); 
    }
}



