import {React} from './../React.js';
import {ReactDom} from './../ReactDom.js';
import {type_check} from './../Functions/type_checker.js';
import {InvalidProps} from './../Exceptions/InvalidProps.js';


export class Router extends React.Component {

    constructor(props) {
        if(!type_check(props, Router.propsType)) throw new InvalidProps();
        super(props);
        let hash = window.location.hash;
        if(hash === "") {
            this.state = {
                page: props.routes[0].path
            }
        } else {
            let hashEdited = hash.substr(1);
            let path = props.routes.find(route => hashEdited === route.path);
            if(path === undefined) {
                this.state = {
                    page: props.routes[0].path
                }
                window.location.hash = "/";
            } else {
                this.state = {
                    page: window.location.hash.substr(1)
                }
            }
        }
        
    }

    watchHash = () => {
        window.addEventListener('hashchange', () => {
            let path = window.location.hash.substr(1);
            this.setState({page: path});
            const parentElement = document.getElementById('content').parentElement;
            document.getElementById('content').remove();
            ReactDom.render(this.display(this.props), parentElement);
        });
    }

    display = (newProps) => {
        if (!type_check(newProps, Router.propsType)) throw new InvalidProps();
        this.checkUrl();
        if (this.needUpdate(newProps, this.state)) {
            this.props = newProps;
        }
        return this.renderContent();
    }

    checkUrl = () => {
        let url = window.location.hash.substr(1);
        let path = this.props.routes.find(route => url === route.path);
        if(path === undefined) {
            this.setState({page: this.props.routes[0].path});
            window.location.hash = "/";
            window.location.reload();
        }
    }

    buttonClicked = (path) => {
        if (this.needUpdate(this.props, {page: path})) {
            this.setState({page: path})
            window.location.hash = path;
        }
    }

    render () {
        this.watchHash();
        return React.createElement("div", {}, [
            React.createElement("nav", { class: "navbar" }, [
                ...this.props.routes.map(route => React.createElement(
                    "button",
                    {click: () => this.buttonClicked(route.path), class: "navbar-button"},
                    [route.content.name]
                ))
            ]),
            React.createElement("div", {id:"content"}, [
                React.createElement(this.props.routes.find(route => this.state.page === route.path).content, {}, [])
            ])
        ]);
    }

    renderContent() {
        return React.createElement("div", {id:"content"}, [
            React.createElement(this.props.routes.find(route => this.state.page === route.path).content, {}, [])
        ]);
    }

}


Router.propsType = {type: 'object', properties:{routes: {type: 'array'}}};

