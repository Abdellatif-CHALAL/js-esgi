import { type_check } from '../Functions/type_checker.js';
import { InvalidProps } from '../Exceptions/InvalidProps.js';
import { React } from '../React.js';
import { ReactDom } from '../ReactDom.js';

export class RequestUsers extends React.Component{
    static id_range = 'range_button';

    constructor(props) {
        if (!type_check(props, RequestUsers.propsType)) throw new InvalidProps();
        super(props);
        this.setState({
            persons: [],
        });
    }

    display = (newProps) => {
        if (!type_check(newProps, RequestUsers.propsType)) throw new InvalidProps();
        if (this.needUpdate(newProps,this.state)) {
            this.props = newProps;
        }
        return this.render();
    }

    onPrint = () => {
        let slider = document.getElementById(this.id_range);
        let xhr = new XMLHttpRequest();
        xhr.open("GET", `https://reqres.in/api/users?per_page=${slider.value}`, false);
        xhr.send(null);
        let persons = JSON.parse(xhr.responseText);

        if (this.needUpdate(this.props, {persons: persons.data})) {
            this.setState({persons: persons.data});
            const parentElement = document.getElementById('request-users-component').parentElement;
            document.getElementById('request-users-component').remove();
            ReactDom.render(this.display(this.props), parentElement);
        }
    }

    render = () => {
        let elements = this.state.persons.map(person => React.createElement('div', {class: 'person-block'}, [
            React.createElement('h2', {}, [person.first_name]),
            React.createElement('p', {}, [person.last_name]),
            React.createElement('p', {}, [person.email]),
            React.createElement('img', {src: person.avatar, class: 'request-users-img'}, []),
        ]));
        return React.createElement('div', {
            id: 'request-users-component'
        }, [
            React.createElement('h1', {class: "title"}, ['Request users API']),
            React.createElement('input', {
                id: this.id_range,
                class: 'range_button',
                type: 'range',
                min: '1',
                max: '10',
                value: '1',
        }, []),
            React.createElement('button', {class: 'search-btn', click: this.onPrint}, ['Appel de l\'API']),
            ...elements,
        ]);
    }
}

RequestUsers.propsType = {type: 'object', properties:{}};