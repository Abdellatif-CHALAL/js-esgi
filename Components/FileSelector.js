import { type_check } from '../Functions/type_checker.js';
import { InvalidProps } from './../Exceptions/InvalidProps.js';
import { React } from './../React.js';
import { ReactDom } from '../ReactDom.js';
import { InvalidFileType } from "../Exceptions/InvalidFileType.js";


export class FileSelector extends React.Component{
    constructor(props){
        if (!type_check(props,FileSelector.propsType)) throw new InvalidProps();
        super(props);
        this.setState({content:''});
    }

    display = (newProps) => {
        if (!type_check(newProps,FileSelector.propsType)) throw new InvalidProps();
        if (this.needUpdate(newProps,this.state)) {
            this.props = newProps;
        }
        return this.render();
    }

    loadData = (e) => {
        const reader = new FileReader();
        reader.addEventListener('load', (event) => {
            this.setState({content:event.target.result});
            const parentElement = document.getElementById('01').parentElement;
            document.getElementById('01').remove();
            ReactDom.render(this.display(this.props),parentElement);
        });

        if(e.target.files[0].type === "text/csv") {
            reader.readAsText(e.target.files[0]);
        }
        else{
            throw new InvalidFileType();
        }
    }

    render = () => {

        const fileinput = React.createElement('input', {
            type: "file",
            class: "file-input",
            change: (e) => {
                try {
                    this.loadData(e);
                }catch (exception){
                    alert('Invalid file type. File must be CSV.');
                }
            }
        }, null);

        if(this.state.content == ''){
            return React.createElement('div',{
                id:'01',
                class:'csv-container'
            },[fileinput]);
        }
        let style;
        let data = new Array();
        let content = this.state.content;
        let lines = content.split("\n");
        for (let i = 0; i < lines.length - 1 ; i++) {
            if(i==0)
                style = "first-row";
            if(i%2 == 1)
                style = "odd-row";
            if(i%2 == 0 && i!=0)
                style = "pair-row";
            let children = new Array();
            let cols = lines[i].split(',');
            for (let j = 0; j < cols.length ; j++) {
                let tmp = React.createElement('td', {
                    style:"border: black solid 2px;"
                }, [cols[j]]);
                children.push(tmp);
            }
            let childrenCopy = [...children];
            data[i] = React.createElement('tr', {
                class:style
            }, childrenCopy);
        }

        const body = React.createElement('tbody', {
        }, data);

        const table = React.createElement('div',{
            class:"table-container"
        },[
            React.createElement('table', {
                style: "border-collapse : collapse;",
                class: "csv-table"
            }, [body])
        ]);



        const cont = React.createElement('div',{
            id:'01',
            class:'csv-container'
        },[
            fileinput,
            table
        ]);

        return cont;
    }
}

FileSelector.propsType = {type: 'object', properties:{}};
