import { React } from '../React.js';
import { ReactDom } from '../ReactDom.js';

import { type_check } from '../Functions/type_checker.js';
import { InvalidProps } from '../Exceptions/InvalidProps.js';

export class MovieDB extends React.Component{

    constructor(props) {
        if (!type_check(props, MovieDB.propsType)) throw new InvalidProps();
        super(props);
        this.setState({
            results: []
        });
    }

    display = (newProps) => {
        if (!type_check(newProps, MovieDB.propsType)) throw new InvalidProps();
        if (this.needUpdate(newProps,this.state)) {
            this.props = newProps;
        }
        return this.render();
    }

    onSearchPressed = () => {
        let inputSearch = document.getElementById('moviedb-input').value;
        let xhr = new XMLHttpRequest();
        const API_TOKEN = "38ffaf67ef1980f0af85d1addd3dbc6d";
        let url = 'https://api.themoviedb.org/3/search/movie?api_key=' + API_TOKEN + '&language=fr&query=' + inputSearch + "&page=1";
        xhr.open("GET", url, false);
        xhr.send(null);
        let data = JSON.parse(xhr.responseText);

        if (this.needUpdate(this.props, {results: data.results})) {
            this.setState({results: data.results});
            const parentElement = document.getElementById('moviedb-component').parentElement;
            document.getElementById('moviedb-component').remove();
            ReactDom.render(this.display(this.props), parentElement);
        }
    }

    render = () => {
        let elements = this.state.results.map(movie => React.createElement('div', {id: 'movie-component'}, [
            React.createElement('img', {src: `https://image.tmdb.org/t/p/w300${movie.poster_path}`, class: 'movie-poster'}, []),
            React.createElement('h1', {}, [movie.original_title]),
            React.createElement('p', {}, [`Date de sortie : ${movie.release_date}`]),
            React.createElement('p', {}, [`Note : ${String(movie.vote_average)}/10`]),
        ]));
        return React.createElement('div', {
            id: 'moviedb-component'
        }, [
            React.createElement('h1', {class: "title"}, ['The MovieDB API']),
            React.createElement('input', {
                id: 'moviedb-input',
                type: 'text',
                placeholder: 'Recherche ...',
            }, []),
            React.createElement('button', {class: 'search-btn', click: this.onSearchPressed}, ['Chercher des films']),
            ...elements,
        ]);
    }
}

MovieDB.propsType = {type: 'object', properties:{}};