import {FileSelector} from './../Components/FileSelector.js';
import {React} from './../React.js';

export class CSVReader extends React.Component{

    render = () => {
        return React.createElement('div', {
            class:"main-container"
        },
            [
                React.createElement('div', {
                        class:"inner-container"
                    },
                    [
                        React.createElement('h1',{class:"title"},["Select a CSV file"]),
                        React.createElement(FileSelector, {}, null)
                    ]
                )
            ]
        );
    }

}


CSVReader.propsType = {type: 'object', properties:{}};
