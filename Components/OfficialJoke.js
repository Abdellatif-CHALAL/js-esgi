import { type_check } from '../Functions/type_checker.js';
import { InvalidProps } from '../Exceptions/InvalidProps.js';
import { React } from '../React.js';
import { ReactDom } from '../ReactDom.js';

export class OfficialJoke extends React.Component{

    constructor(props) {
        if (!type_check(props, OfficialJoke.propsType)) throw new InvalidProps();
        super(props);
        this.setState({
            jokes: {
                setup: " ",
                punchline: " "
            },
        });
    }

    display = (newProps) => {
        if (!type_check(newProps, OfficialJoke.propsType)) throw new InvalidProps();
        if (this.needUpdate(newProps,this.state)) {
            this.props = newProps;
        }
        return this.render();
    }

    onRandom = () => {
        let xhr = new XMLHttpRequest();
        xhr.open("GET", `https://official-joke-api.appspot.com/jokes/random`, false);
        xhr.send(null);
        let jokes = JSON.parse(xhr.responseText);
        
        if (this.needUpdate(this.props, {jokes: jokes})) {
            this.setState({jokes: jokes});
            const parentElement = document.getElementById('jokeComponent').parentElement;
            document.getElementById('jokeComponent').remove();
            ReactDom.render(this.display(this.props), parentElement);
        }
    }
    
    onSpecificJoke = (type) => {
        let xhr = new XMLHttpRequest();
        xhr.open("GET", `https://official-joke-api.appspot.com/jokes/${type}/random`, false);
        xhr.send(null);
        let jokes = JSON.parse(xhr.responseText);

        if (this.needUpdate(this.props, {jokes: jokes[0]})) {
            this.setState({jokes: jokes[0]});
            const parentElement = document.getElementById('jokeComponent').parentElement;
            document.getElementById('jokeComponent').remove();
            ReactDom.render(this.display(this.props), parentElement);
        }
    }

    render = () => {
        return React.createElement('div', {id: 'jokeComponent'}, [
            React.createElement('h1', {class: "title"}, ['API de blagues']),
            React.createElement('input', {class: 'joke-search-btn', type: 'button', value: 'Random', click: this.onRandom, style: 'display: block;'}),
            React.createElement('input', {type: 'button', class: 'joke-search-btn', value: 'General type', click: () => this.onSpecificJoke("general"), style: 'display: block;'}),
            React.createElement('input', {type: 'button', class: 'joke-search-btn', value: 'Knock-knock type', click: () => this.onSpecificJoke("knock-knock"), style: 'display: block;'}),
            React.createElement('input', {type: 'button', class: 'joke-search-btn', value: 'Programming type', click: () => this.onSpecificJoke("programming"), style: 'display: block;'}),
            React.createElement('p', {}, [
                this.state.jokes.setup
            ]),
            React.createElement('p', {}, [
                this.state.jokes.punchline
            ]),
        ])
    }
}

OfficialJoke.propsType = {type: 'object', properties:{}};