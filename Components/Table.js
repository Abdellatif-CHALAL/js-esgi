import {type_check} from './../Functions/type_checker.js';
import {InvalidProps} from './../Exceptions/InvalidProps.js';
import {React} from './../React.js';
import { ReactDom } from '../ReactDom.js';


export class Table extends React.Component{
    constructor(props){
        if (!type_check(props,Table.propsType)) throw new InvalidProps();
        super(props);
        this.setState({});
    }

    display = (newProps) => {
        if (!type_check(newProps,Table.propsType)) throw new InvalidProps();
        if (this.needUpdate(newProps,this.state)) {
            this.props = newProps;
        }
        return this.render();
    }

    render = () => {
        return React.createElement('table', {
            id: "01"
        }, [
            React.createElement('th', {
            }, [React.createElement('td',null, [])],React.createElement('td',null, []))

        ]);
    }
}

Table.propsType = {type: 'object', properties:{}};