import {ReactDom} from './ReactDom.js';
import {React} from './React.js';
import {Router} from './Components/Router.js';
import {CSVReader} from './Components/CSVReader.js';
import {Home} from './Components/Home.js';
import { OfficialJoke } from './Components/OfficialJoke.js';
import { RequestUsers } from './Components/RequestUsers.js';
import { MovieDB } from './Components/MovieDB.js';


// Navbar and Router
ReactDom.render(
    React.createElement("div", {}, [
        React.createElement(Router, {
            routes: [
                {path: "/", content: Home},
                {path: "/requestusers", content: RequestUsers},
                {path: "/moviedb", content: MovieDB},
                {path: "/officialjoke", content: OfficialJoke},
                {path: "/csvreader", content: CSVReader}

            ]
        }, null)
    ]),
    document.getElementById('root')
);


