import {UndefinedPropertyError} from './../Exceptions/UndefinedPropertyError.js';
export const prop_access = (obj, path) => {
    if(path === "" || path == null || obj == null){
        throw new UndefinedPropertyError(path);
    }
    let props = path.split('.');
    let result = obj;
    for (var i = 0; i < props.length; i++) {
      result = result[props[i]];
      if (typeof result === 'undefined'){
        throw new UndefinedPropertyError(path);
      }
    }
    return result;
  }