import {prop_access} from './prop_access.js';
export default String.prototype.interpolate = function(props){
    if(props === null) return;
    let array = this.match(/\{\{.*?\}}|.+?(?=\{{|$)/g);
    if(array === null) return this;
    array = array.map(index => index.trim());
    for (const index in array) {
        if((array[index].slice(0,2) === "{{") && (array[index].slice(array[index].length-2,array[index].length) === "}}")){
            const value = array[index].slice(2,array[index].length-2).trim();
            array[index] = prop_access(props,value);
        }
    }
    return array.join(' ');
}

