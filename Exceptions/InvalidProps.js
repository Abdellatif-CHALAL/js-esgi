export function InvalidProps(){
    this.name = "InvalidProps";
    var instance = new Error("Exception details: Invalid props");
    Object.setPrototypeOf(instance,Object.getPrototypeOf(this));
    if (Error.captureStackTrace) {
      Error.captureStackTrace(instance,InvalidProps);
    }
    return instance;
}