export function UndefinedPropertyError(path) {
    this.name = "UndefinedPropertyError";
    var instance = new Error(`Exception details: The path "${path}" dosen't existe`);
    Object.setPrototypeOf(instance,Object.getPrototypeOf(this));
    if (Error.captureStackTrace) {
      Error.captureStackTrace(instance,UndefinedPropertyError);
    }
    return instance;
  }