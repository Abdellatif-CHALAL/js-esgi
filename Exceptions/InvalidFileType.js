export function InvalidFileType(){
    this.name = "InvalidFileType";
    var instance = new Error("Exception details: Invalid file type. File must be CSV.");
    Object.setPrototypeOf(instance,Object.getPrototypeOf(this));
    if (Error.captureStackTrace) {
        Error.captureStackTrace(instance,InvalidFileType);
    }
    return instance;
}